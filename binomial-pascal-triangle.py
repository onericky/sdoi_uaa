grade = 20
triangle = []

def generatePascalTriangle():
	for i in range(grade):
		triangle.append([])
		if i == 0:
			triangle[i].append(1)
		elif i == 1:
			triangle[i].append([1, 1])
		else:
			aux = []
			for j in range(len(triangle[i-1][0])):
				if j == 0:
					aux.append(1)
				else:
					aux.append(triangle[i-1][0][j-1] + triangle[i-1][0][j])
			aux.append(1)
			triangle[i].append(aux)


def printPascalTriangle():
	for i in range(grade):
		print(triangle[i][0])


if __name__ == '__main__':
	while True:
		try:
			grade = int(input('Grade: '))
		except ValueError:
			print('  ** Grade must be int')
			continue
		else:
			break
			
	generatePascalTriangle()
	printPascalTriangle()
    