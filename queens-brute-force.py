# Brute force - Queens problem
import numpy as np

queens = 5
valueQueen = 123


def algorithm():
    solutions = list()
    for i in range(queens**2):
        board = np.zeros((queens, queens))
        missing = queens
        attempt = i
        if i + 1 >= queens ** 2:
            attempt = (((i + 1) // queens) // queens) - 1

        [board, missing] = assign(board, queens, 0, attempt, 0)
        if missing == 0:
            if validateSolution(solutions, board.tolist()):
                solutions.append(board.tolist())

    return solutions


def assign(board, missing, correct, attempt, auxAttempt):
    if missing <= 0:
        return [board, missing]

    for x in range(queens):
        for y in range(queens):
            auxAttempt += 1
            if auxAttempt <= attempt:
                continue

            if board[x][y] != valueQueen:
                board[x][y] = valueQueen
                test = validatePosition(board, correct)
                if test == False:
                    board[x][y] = 0
                    continue
                return assign(board, missing - 1, correct + 1, attempt, auxAttempt)

    return [board, missing]


def validatePosition(board, correct):
    if correct == 0:
        return True

    qx = ['' for i in range(queens)]
    qy = ['' for i in range(queens)]
    aux = 0

    for x in range(queens):
        for y in range(queens):
            if board[x][y] == valueQueen:
                qx[aux] = x
                qy[aux] = y
                aux += 1

    qxAux = []
    for i in qx:
        if i != '':
            qxAux.append(i)

    qyAux = []
    for i in qy:
        if i != '':
            qyAux.append(i)

    # validate in x
    duplicateX = any(qxAux.count(element) > 1 for element in qxAux)
    # validate in y
    duplicateY = any(qyAux.count(element) > 1 for element in qyAux)

    if duplicateX or duplicateY:
        return False
    if duplicateX == False and duplicateY == False:
        # validate in diagonal
        return validateDiagonal(board, correct)


def validateDiagonal(board, correct):
    for x in range(len(board)):
        for y in range(len(board)):
            if board[x][y] == valueQueen:
                auxX = x
                auxY = y
                # to right and down
                if auxX + 1 < queens and auxY + 1 < queens:
                    while auxX + 1 < queens and auxY + 1 < queens:
                        auxX += 1
                        auxY += 1
                        if board[auxX][auxY] == valueQueen:
                            return False

                auxX = x
                auxY = y
                # to left and down
                if auxX + 1 < queens and auxY > 0:
                    while auxX + 1 < queens and auxY > 0:
                        auxX += 1
                        auxY -= 1
                        if board[auxX][auxY] == valueQueen:
                            return False

                auxX = x
                auxY = y
                # to right and up
                if auxX > 0 and auxY + 1 < queens:
                    while auxX > 0 and auxY + 1 < queens:
                        auxX -= 1
                        auxY += 1
                        if board[auxX][auxY] == valueQueen:
                            return False

                auxX = x
                auxY = y
                # to left and up
                if auxX > 0 and auxY > 0:
                    while auxX > 0 and auxY > 0:
                        auxX -= 1
                        auxY -= 1
                        if board[auxX][auxY] == valueQueen:
                            return False

    return True


def validateSolution(solutions, board):
    if not solutions:
        return True
    else:
        for a in range(len(solutions)):
            if solutions[a] == board:
                return False
    return True


def printSolutions(solutions):
    auxSolutions = list()
    for a in range(len(solutions)):
        print('/*--------------------------- ' + str(a) + ' -----------------------------*/')
        for x in range(queens):
            for y in range(queens):
                if solutions[a][x][y] == 123:
                    solutions[a][x][y] = 'Q'
                else:
                    solutions[a][x][y] = '-'
            print(solutions[a][x])


if __name__ == '__main__':
    solutions = algorithm()
    printSolutions(solutions)